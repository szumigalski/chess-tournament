import React from 'react';
import {AsyncStorage} from 'react-native';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import MainPageUnregister from './components/MainPageUnregister';
import MainPageRegisterPanel from './components/MainPageRegisterPanel';
import AccountNavigator from './components/Account/AccountNavigator';
import MainPageLoginPanel from './components/MainPageLoginPanel';
import ExistingTournaments from './components/ExistingTournaments';
import AccountPanel from './components/Account/AccountPanel';
import MainPageSuccessLogin from './components/Account/MainPageSuccessLogin';

export const AuthContext = React.createContext()

const initialState = {
  isAuthenticated: false,
  userName: null,
  token: null,
}

const reducer = (state, action) => {
  switch (action.type) {
    case "LOGIN":
      AsyncStorage.setItem("userName", JSON.stringify(action.payload.userName));
      AsyncStorage.setItem("token", JSON.stringify(action.payload.token));
      return {
        ...state,
        isAuthenticated: true,
        userName: action.payload.userName,
        token: action.payload.token
      };
    case "LOGOUT":
      AsyncStorage.clear();
      return {
        ...state,
        isAuthenticated: false,
        userName: null
      };
    default:
      return state;
  }
}

const App = () => {
  const [state, dispatch] = React.useReducer(reducer, initialState)
  return(
    <MainPageUnregister />
)
};

const AppNavigator = createStackNavigator(
  {
    App: App,
    ExistingTournaments: ExistingTournaments,
    MainPageRegisterPanel: MainPageRegisterPanel,
    AccountNavigator: {
      screen: AccountNavigator,
      navigationOptions: {
        header: null,
      },
    },
    MainPageLoginPanel: MainPageLoginPanel,
    AccountPanel: {
      screen: AccountPanel,
      navigationOptions: {
        header: null,
      },
    },
    MainPageSuccessLogin: {
      screen: MainPageSuccessLogin,
      navigationOptions: {
        header: null,
      },
    },
  },
  {
    initialRouteName: 'App',
  },
);

export default createAppContainer(AppNavigator);
