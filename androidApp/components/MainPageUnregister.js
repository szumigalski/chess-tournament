import React from 'react';
import {View} from 'native-base';
import Layout, {StyledButton, StyledText} from '../layouts/Layout';
import {withNavigation} from 'react-navigation';

const MainPageUnregister = ({navigation}) => {
  return (
    <Layout headers={['Chess', 'Tournament']} headersSize={45}>
      <View>
        <StyledButton
          marginBottom={30}
          onPress={() => navigation.navigate('MainPageLoginPanel')}>
          <StyledText>Login</StyledText>
        </StyledButton>
        <StyledButton
          onPress={() => navigation.navigate('MainPageRegisterPanel')}>
          <StyledText>Register</StyledText>
        </StyledButton>
      </View>
    </Layout>
  );
};
export default withNavigation(MainPageUnregister);
