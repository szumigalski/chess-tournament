import React, { useState } from 'react';
import {Form, Content} from 'native-base';
import Layout, {
  WhiteSegment,
  StyledButton,
  StyledText,
  FormItem,
  MontInput,
  MontLabel,
} from '../layouts/Layout';
import { gql } from 'apollo-boost';
import {withNavigation} from 'react-navigation';
import withProvider from './withProvider'
import {AsyncStorage} from 'react-native';

const LOGIN = gql`
        query login($name: String!, $password: String!){
            login(name: $name, password: $password) {
                token
            }
        }
        `;

const MainPageLoginPanel = ({navigation, client}) => {
  const [name, setName] = useState('szumi')
  const [password, setPassword] = useState('szumi')

  const tryLogin = () => {
    client.query({
      query: LOGIN,
      variables:{name: name, password: password}
    })
    .then(() => {
      navigation.navigate('AccountNavigator');
      AsyncStorage.setItem("userName", JSON.stringify(action.payload.userName));
      AsyncStorage.setItem("token", JSON.stringify(action.payload.token));
    })
}

  return (
    <Layout headers={['Login']} headersSize={45}>
      <WhiteSegment height="80">
        <Content>
          <Form>
            <FormItem stackedLabel>
              <MontLabel>Name</MontLabel>
              <MontInput
                onChangeText={text => setName(text)}
                value={name}
                placeholder="Enter name..."
                />
            </FormItem>
            <FormItem stackedLabel>
              <MontLabel>Password</MontLabel>
              <MontInput
                onChangeText={text => setPassword(text)}
                value={password}
                placeholder="Enter password..."
                secureTextEntry={true}
              />
            </FormItem>
          </Form>
        </Content>
            <StyledButton
              onPress={() => tryLogin()}
              >
              <StyledText>Login</StyledText>
            </StyledButton>
      </WhiteSegment>
    </Layout>
  );
};
export default withProvider(withNavigation(MainPageLoginPanel));
