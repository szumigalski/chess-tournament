import React from 'react';
import {Form, Content} from 'native-base';
import Layout, {
  WhiteSegment,
  StyledButton,
  StyledText,
  FormItem,
  MontInput,
  MontLabel,
} from '../layouts/Layout';
import {withNavigation} from 'react-navigation';

const MainPageRegisterPanel = ({navigation}) => {
  return (
    <Layout headers={['Register']} headersSize={45}>
      <WhiteSegment height="80">
        <Content>
          <Form>
            <FormItem stackedLabel>
              <MontLabel>Name</MontLabel>
              <MontInput placeholder="Enter name..." />
            </FormItem>
            <FormItem stackedLabel>
              <MontLabel>Password</MontLabel>
              <MontInput
                placeholder="Enter password..."
                secureTextEntry={true}
              />
            </FormItem>
            <FormItem stackedLabel>
              <MontLabel>Category (default 1000)</MontLabel>
              <MontInput placeholder="Enter category..." />
            </FormItem>
          </Form>
        </Content>
        <StyledButton onPress={() => navigation.navigate('AccountNavigator')}>
          <StyledText>Create account</StyledText>
        </StyledButton>
      </WhiteSegment>
    </Layout>
  );
};
export default withNavigation(MainPageRegisterPanel);
