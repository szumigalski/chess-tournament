import React from 'react';
import {Text, View} from 'native-base';
import styled from 'styled-components/native';
import Layout from '../layouts/Layout';

const StyledText = styled(Text)`
  color: black;
  font-family: 'Montserrat-Bold';
`;

const WhiteContainer = styled(View)`
  height: 100%;
  background-color: rgba(255, 255, 255, 0.5);
  margin-bottom: 15px;
`;

const ExistingTournaments = () => {
  return (
    <>
      <Layout headers={['Existing', 'Tournaments']} headersSize={40}>
        <WhiteContainer>
          <StyledText>lol</StyledText>
        </WhiteContainer>
      </Layout>
    </>
  );
};

export default ExistingTournaments;
