import React from 'react'
import { ApolloProvider } from '@apollo/react-hooks'
import ApolloClient from 'apollo-boost'
import {AsyncStorage} from 'react-native';

const withProvider = (Component,navigationOptions)=>{
    const client = new ApolloClient({
        uri: 'http://192.168.0.101:5000/graphql',
        request: (operation) => {
          const token = AsyncStorage.getItem('token')
          if(token) {
              console.log('token', token)

          }
        }
      })
   return class extends React.Component {
       static navigationOptions = navigationOptions;
       render(){
           return(
               <ApolloProvider client={client}>
                   <Component props={this.props} client={client}/>
               </ApolloProvider>
               )
           }
       }
   }
export default withProvider