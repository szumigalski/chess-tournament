import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import {createAppContainer} from 'react-navigation';
import {createDrawerNavigator} from 'react-navigation-drawer';
import {createStackNavigator} from 'react-navigation-stack';
import Icon from 'react-native-vector-icons/Ionicons';
import AccountPanel from './AccountPanel';
import MainPageSuccessLogin from './MainPageSuccessLogin';

const NavigationDrawerStructure = ({navigationProps}) => {
  //Structure for the navigatin Drawer
  const toggleDrawer = () => {
    //Props to open/close the drawer
    navigationProps.toggleDrawer();
  };
  return (
    <View style={{flexDirection: 'row', marginRight: 20}}>
      <TouchableOpacity onPress={toggleDrawer.bind(this)}>
        <Icon name="md-menu" size={30} color="#000" />
      </TouchableOpacity>
    </View>
  );
};

const AccountPanel_StackNavigator = createStackNavigator({
  First: {
    screen: AccountPanel,
    navigationOptions: ({navigation}) => ({
      title: '',
      headerRight: <NavigationDrawerStructure navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: 'fff',
      },
      headerTintColor: '#fff',
    }),
  },
});

export const MainPageSuccessLogin_StackNavigator = createStackNavigator({
  Second: {
    screen: MainPageSuccessLogin,
    navigationOptions: ({navigation}) => ({
      title: '',
      headerRight: <NavigationDrawerStructure navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#fff',
      },
      headerTintColor: '#fff',
    }),
  },
});

const AccountNavigator = createDrawerNavigator(
  {
    MainPageSuccessLogin_StackNavigator: {
      //Title
      screen: MainPageSuccessLogin_StackNavigator,
      navigationOptions: {
        drawerLabel: 'Start tournament',
      },
    },
    AccountPanel_StackNavigator: {
      //Title
      screen: AccountPanel_StackNavigator,
      navigationOptions: {
        drawerLabel: 'Account Panel',
      },
    },
  },
  {
    initialRouteName: 'MainPageSuccessLogin_StackNavigator',
  },
);

export default createAppContainer(AccountNavigator);
