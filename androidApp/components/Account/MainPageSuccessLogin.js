import React from 'react';
import {View} from 'native-base';
import Layout, {StyledButton, StyledText} from '../../layouts/Layout';

const MainPageSuccessLogin = ({navigation}) => {
  return (
    <>
      <Layout headers={['Chess', 'Tournament']} headersSize={45}>
        <View>
          <StyledButton marginBottom={30}>
            <StyledText>Start new tournament</StyledText>
          </StyledButton>
          <StyledButton
            onPress={() => navigation.navigate('ExistingTournaments')}>
            <StyledText>Join existing tournament</StyledText>
          </StyledButton>
        </View>
      </Layout>
    </>
  );
};

export default MainPageSuccessLogin;
