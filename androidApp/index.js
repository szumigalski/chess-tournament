/**
 * @format
 */
//export ANDROID_HOME=/usr/local/share/android-sdk
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
