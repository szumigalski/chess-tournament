import React from 'react';
import {Button, Text, View, Item, Input, Label} from 'native-base';
import styled from 'styled-components/native';
import background from '../images/background.png';

const MontText = styled.Text`
  font-family: 'Montserrat-Bold';
  font-size: ${({headersSize}) => headersSize + 'px'};
  color: white;
`;

const StyledBackground = styled.ImageBackground`
  width: 100%;
  height: 100%;
  padding: 20px;
  flex-direction: column;
  justify-content: space-between;
`;

export const StyledButton = styled(Button)`
  background-color: white;
  justify-content: center;
  margin-bottom: ${({marginBottom}) => (marginBottom ? marginBottom : 0)};
`;

export const StyledText = styled(Text)`
  color: black;
  font-family: 'Montserrat-Bold';
`;

export const WhiteSegment = styled(View)`
  background-color: rgba(255, 255, 255, 0.75);
  border-radius: 5px;
  padding: 15px;
  height: ${({height}) => (height ? `${height}%` : '100%')};
  margin: 25px 5px;
`;

export const FormItem = styled(Item)`
  border-color: black;
`;

export const MontInput = styled(Input)`
  font-family: 'Montserrat-Regular';
`;

export const MontLabel = styled(Label)`
  font-family: 'Montserrat-SemiBold';
`;

const Layout = ({headers, children, headersSize}) => {
  return (
    <>
      <StyledBackground source={background}>
        <View>
          {headers.map((header, i) => (
            <MontText key={i} headersSize={headersSize}>
              {header}
            </MontText>
          ))}
        </View>
        {children}
      </StyledBackground>
    </>
  );
};

export default Layout;
