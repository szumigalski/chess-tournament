## Rozpisanie funkcjonalności

### Aplikacja mobilna

Użytkownicy aplikacji mobilnej dzielą się na 2 typy

 - Organizator turnieju
 - Uczestnicy turnieju

#### Organizator turnieju

 - [ ] Ustala godziny rozpoczęcia turnieju, oraz zakończenie zapisów (powinno być możliwe ręczne zamknięcie zapisów)
 - [ ] Spisuje zasady turnieju widoczne dla uczestników (liczba rund, czas na zawodnika itd.) Zakładamy oczywiście system szwajcarski, oraz Bucholz (+ mały Bucholz, w przyszłości możliwe dodatkowe systemy, jednak ten najpopularniejszy)
 - [ ] Nadaje uprawnienia innym graczom (organizator jest również sędzią)
 - [ ] Zatwierdza wyniki partii wysłane przez użytkowników, może je dowolnie modyfikować
 
 #### Uczestnik turnieju
 
 - [ ] Zapisuje się na turniej
 - [ ] Otrzymuje powiadomienia o nowej partii, z kim gra na jakiej szachownicy
 - [ ] Otrzymuje powiadomienia o zmianach jakie sędzie wprowadza w jego partii
 - [ ] Ma dostęp do aktualnej tabeli wyników oraz do zasad turnieju spisanych przez organizatora 
 
 ## Aplikacja desktopowa

Narzędzie dla sędziów i organizatorów. Widać na nim lepiej szczegóły zawodników. Posiada mniejszy priorytet (możliwe, że aplikacja mobilna wystarczy do tak prostych edycji). Potem jednak może się przydać jakby się chciało np. drukować wyniki

## Model bazy danych