import React from 'react';
import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom'
import AuthPage from './components/Auth'
import Tournaments from './components/Tournaments'
import { ApolloProvider } from '@apollo/react-hooks'
import ApolloClient from 'apollo-boost'

export const AuthContext = React.createContext()

const initialState = {
  isAuthenticated: false,
  userName: null,
  token: null,
}

const reducer = (state, action) => {
  switch (action.type) {
    case "LOGIN":
      localStorage.setItem("userName", JSON.stringify(action.payload.userName));
      localStorage.setItem("token", JSON.stringify(action.payload.token));
      return {
        ...state,
        isAuthenticated: true,
        userName: action.payload.userName,
        token: action.payload.token
      };
    case "LOGOUT":
      localStorage.clear();
      return {
        ...state,
        isAuthenticated: false,
        userName: null
      };
    default:
      return state;
  }
}

const client = new ApolloClient({
  uri: 'http://localhost:5000/graphql',
  request: (operation) => {
    const token = localStorage.getItem('token')
    operation.setContext({
      headers: {
        authorization: token ? `Bearer ${token.split('"').join('')}` : ''
      }
    })
  }
})

function App() {
  const [state, dispatch] = React.useReducer(reducer, initialState)
  return (
    <ApolloProvider client={client}>
      <AuthContext.Provider
            value={{
              dispatch,
              state
            }}
          >
        <BrowserRouter>
          <Switch>
              <Redirect from='/' to='auth' exact />
              <Route path='/auth' component={AuthPage} />
              <Route path='/tournaments' component={Tournaments} />
          </Switch>
        </BrowserRouter>
      </AuthContext.Provider>
    </ApolloProvider>
  );
}

export default App;
