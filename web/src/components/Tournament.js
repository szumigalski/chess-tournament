import React, { useState, useEffect } from 'react'
import { Table, Header, Button, Dropdown, Segment, Tab } from 'semantic-ui-react'
import { useMutation } from '@apollo/react-hooks'
import { gql } from 'apollo-boost'
import _ from 'lodash'

const ADD_PLAYER = gql`
mutation createPlayer($user: String $tournament: String) {
    createPlayer(playerInput: {user: $user tournament: $tournament}) {
    user {
        name
    }
    tournament {
        _id
    }
    _id
  }
}
`

const ADD_CHESSGAME = gql`
mutation createChessGame($white: String $black: String $tournament: String $chessboard: Int $roundNumber: Int ) {
    createChessGame(chessgameInput: {white: $white black: $black tournament: $tournament chessboard: $chessboard roundNumber: $roundNumber}) {
        _id
    }
}
`

const UPDATE_CHESSGAME = gql`
mutation updateChessGame($_id: String $result: String) {
    updateChessGame(chessgameUpdate: {_id: $_id result: $result}) {
        result
    }
}
`


const Tournament = ({ tournament, refetch, users }) => {
    const [user, setUser] = useState(null)
    const [addPlayer, { name: playername }] = useMutation(ADD_PLAYER)
    const [addChessgame, { name: chessgamename }] = useMutation(ADD_CHESSGAME)
    const [updateChessgame, { name: chessname }] = useMutation(UPDATE_CHESSGAME)
    const [stateUsers, setStateUsers] = useState([])

    useEffect(() => {
        let localUsers = []
        users.map((user, i) => {
            const isValid = findUserInTournament(user._id)
            isValid && localUsers.push({
                key: i,
                value: user._id,
                text: user.name,
                category: user.category
            })
        })
        setStateUsers(_.sortBy(localUsers, ['category']))
    }, [users, tournament])

    const findUserInTournament = (id) => {
        let isValid = true
        tournament.participants.map(p => {
            p.user._id === id && (isValid = false)
        })
        return isValid
    }

    const getRoundList = (list, round) => {
        let newList = []
        let half = list.length/2
        if(round === 1) {
            for(let i=0; i<half; i+=1) {
                i%2 === 0 ?
                newList.push([list[i], list[i+half]]) :
                newList.push([list[i+half], list[i]])
            }
        }
        return newList
    }

    const addNewPlayer = (userId) => {
        addPlayer({ variables: {user: userId, tournament: tournament._id}})
        .then(() => refetch()
            .then(() => setUser(null)))
    }

    const renderpanes = () => {
        let panes = []
        for(let i=1; i<8; i+=1) {
            panes.push({
                menuItem: `Round ${i}`,
                render: () => <Tab.Pane loading>Tab {i} Content</Tab.Pane>,
            })
        }
        return panes
    }

    return(<div style={{display: 'flex', height: '100%'}}>
        <Segment style={{width: 350, margin: 0, height: '100%'}}>
            <Header as='h3'>Tournament {tournament && tournament.name}</Header>
            <Table celled selectable style={{width: 300}}>
                <Table.Header>
                <Table.Row>
                    <Table.HeaderCell>Participants</Table.HeaderCell>
                    <Table.HeaderCell>Category</Table.HeaderCell>
                </Table.Row>
                </Table.Header>
                <Table.Body>
                {tournament && tournament.participants
                    ? tournament.participants.map(p =>
                    <Table.Row>
                    <Table.Cell>{p.user.name}</Table.Cell>
                    <Table.Cell>{p.user.category ? p.user.category : "1000"}</Table.Cell>
                </Table.Row>)
                : ''}
                </Table.Body>
            </Table>
            <Dropdown
                selection
                style={{width: 300}}
                options={stateUsers}
                placeholder='Choose players'
                value={user}
                onChange={(e, { value }) => setUser(value)}
            />
            <Button disabled={!user} onClick={() => addNewPlayer(user)}>Add Player</Button>
        </Segment>
        <Segment style={{width: '100%', margin: 0, height: '100%'}}>
            <Tab panes={renderpanes()} />
        </Segment>
    </div>)
}

export default Tournament