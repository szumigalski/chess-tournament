import React, { useState, useEffect } from 'react'
import { AuthContext } from '../App'
import { gql } from 'apollo-boost'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { Table, Segment, Button } from 'semantic-ui-react'
import Tournament from './Tournament'
import _ from 'lodash'

const categories = [
    1000, 1000, 1000, 1000,
    1200, 1200, 1200,
    1400, 1400,
    1600, 1600, 1800, 2000]

const TOURNAMENTS = gql`
        query {
                tournaments {
                    _id
                    name
                    countOfRounds
                    organizator {
                        password
                        category
                        name
                    }
                    participants {
                        user {
                            _id
                            name
                            category
                        }
                    }
                }
                users {
                    _id
                    name
                    category
                    players {
                        _id
                    }
                }
            }
        `

const ADD_USER = gql`
mutation createUser($name: String! $password: String! $category: Int) {
    createUser(userInput: {name: $name password: $password category: $category}) {
    name
  }
}
`

const ADD_TOURNAMENT = gql`
mutation createTournament($organizator: String! $name: String) {
    createTournament(tournamentInput: {organizator: $organizator name: $name}) {
    _id
  }
}
`

const Tournaments = () => {
    const [ tournament, setTournament] = useState(null)
    const { state: authState } = React.useContext(AuthContext)
    const { loading, tournamentError, data, refetch, networkStatus } = useQuery(TOURNAMENTS)
    const [addUser, { name: username }] = useMutation(ADD_USER)
    const [addTournament, { name: tournamentname }] = useMutation(ADD_TOURNAMENT)

    useEffect(() => {
        tournament && setTournament(_.find(data.tournaments, { _id: tournament._id}))
    }, [data])

    const addNewUser = () => {
        addUser({ variables: {
            name: "player "+data.users.length,
            password: "player "+data.users.length,
            category: categories[Math.floor(Math.random()*categories.length)]
        }})
        .then(() => refetch())
    }

    const addNewTournament = () => {
        addTournament({ variables: {organizator: data.users[0]._id.toString(), name: "Tournament" +(data.tournaments.length+1)}})
        .then(() => refetch())
    }

    return (
        <div style={{display: "flex"}}>
            <Segment style={{width: '30%', height: '100vh', margin: 0}}>
            <h1>Tournaments {networkStatus}</h1>
            <div style={{width: 300, height: '35%', overflow: 'auto'}}>
                <Table celled selectable>
                    <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Tournament</Table.HeaderCell>
                        <Table.HeaderCell>Organizator</Table.HeaderCell>
                    </Table.Row>
                    </Table.Header>
                    <Table.Body>
                    {data && data.tournaments
                    ? data.tournaments.map(tournament =>
                        <Table.Row onClick={() => setTournament(tournament)}>
                        <Table.Cell>{tournament.name}</Table.Cell>
                        <Table.Cell>{tournament.organizator.name}</Table.Cell>
                    </Table.Row>)
                    : ''}
                    </Table.Body>
                </Table>
            </div>
            <Button onClick={() => addNewTournament()} loading={loading}>Add Tournament</Button>
            <div>
            </div>
            <h1>Users</h1>
            <div style={{width: 300, height: '35%', overflow: 'auto'}}>
                <Table celled selectable style={{width: 300, height: '50%', overflow: 'auto'}}>
                    <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Users</Table.HeaderCell>
                    </Table.Row>
                    </Table.Header>
                    <Table.Body>
                    {data && data.users
                    ? data.users.map(user =>
                        <Table.Row>
                        <Table.Cell>{user.name}</Table.Cell>
                    </Table.Row>)
                    : ''}
                    </Table.Body>
                </Table>
            </div>
            <Button onClick={() => addNewUser()} loading={loading}>Add User</Button>
            </Segment>
            {tournament && <Segment style={{width: '70%', height: '100vh', margin: 0}}>
                <Tournament tournament={tournament} refetch={refetch} users={data.users}/>
            </Segment>}
        </div>
        )
}

export default Tournaments