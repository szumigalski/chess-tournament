import React, { useState } from 'react'
import { Segment, Input, Header, Button } from 'semantic-ui-react'
import { ApolloConsumer } from 'react-apollo';
import { gql } from 'apollo-boost'
import { useHistory } from 'react-router-dom'
import { AuthContext } from '../App'

const Auth = () => {
    const { dispatch } = React.useContext(AuthContext)
    const [name, setName] = useState('szumi')
    const [password, setPassword] = useState('szumi')

    let history = useHistory()

    const LOGIN = gql`
        query Login($name: String!, $password: String!){
            login(name: $name, password: $password) {
                token
            }
        }
        `
    const tryLogin = (data) => {
        if(data.login && data.login.token) {
            history.push('/tournaments')
            dispatch({
                type: "LOGIN",
                payload: {
                    userName: name,
                    token: data.login.token
                }
            })
        } else {
            console.log('error')
        }
    }

    return (
        <div style={{height: '100vh', display: 'flex', alignItems: 'center'}}>
            <Segment style={{margin: 'auto', width: 500, display: 'flex', flexDirection: 'column'}}>
                <h1>Auth Page</h1>
                <Header as='h3'>Name</Header>
                <Input
                    style={{width: 300}}
                    value={name}
                    onChange={({target: {value}}) => setName(value)}
                    />
                <Header as='h3'>Password</Header>
                <Input
                    value={password}
                    onChange={({target: {value}}) => setPassword(value)}
                    style={{width: 300}}
                    />
                <ApolloConsumer>
                    {client => (
                        <Button
                        positive
                        style={{marginTop: 20}}
                        onClick={async () => {
                            const { data } = await client.query({
                            query: LOGIN,
                            variables: { name, password }
                            });
                            tryLogin(data)
                        }}
                        >
                        Login
                        </Button>
                    )}
                </ApolloConsumer>
            </Segment>
        </div>
    )
}

export default Auth