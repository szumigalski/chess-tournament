const mongoose = require('mongoose')

const Schema = mongoose.Schema

const tournamentSchema = new Schema({
    name: {
        type: String,
        required: false
    },
    place: {
        type: String,
        required: false
    },
    contact: {
        type: String,
        required: false
    },
    countOfRounds: {
        type: Number,
        required: false
    },
    timeForPlayers: {
        type: Number,
        required: false
    },
    limitOfPlayers: {
        type: Number,
        required: false
    },
    otherInformations: {
        type: String,
        required: false
    },
    currentRound: {
        type: Number,
        required: false
    },
    date: {
        type: Date,
        required: false
    },
    organizator: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    participants: [{
        type: Schema.Types.ObjectId,
        ref: 'Player'
    }],
    newRoundTime: {
        type: String,
        required: false
    },
    chessgames: [{
        type: Schema.Types.ObjectId,
        ref: 'Chessgame'
    }]
})

module.exports = mongoose.model('Tournament', tournamentSchema)