const mongoose = require('mongoose')

const Schema = mongoose.Schema

const userSchema = new Schema({
    password: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true,
        unique: true
    },
    category: {
        type: Number,
        required: false
    },
    players: [{
            type: Schema.Types.ObjectId,
            ref: 'Player'
        }],
    createdTournaments: [{
        type: Schema.Types.ObjectId,
        ref: 'Tournament'
    }]
})

module.exports = mongoose.model('User', userSchema)