const mongoose = require('mongoose')

const Schema = mongoose.Schema

const chessgameSchema = new Schema({
    tournament: {
        type: Schema.Types.ObjectId,
        ref: 'Tournament',
        required: true
    },
    white: {
        type: Schema.Types.ObjectId,
        ref: 'Player',
        required: false
    },
    black: {
        type: Schema.Types.ObjectId,
        ref: 'Player',
        required: false
    },
    roundNumber: {
        type: Number,
        required: false
    },
    whiteResult: {
        type: String,
        required: false
    },
    blackResult: {
        type: String,
        required: false
    },
    result: {
        type: String,
        required: false
    },
    chessboardNumber: {
        type: Number,
        required: false
    }
})

module.exports = mongoose.model('Chessgame', chessgameSchema)