const mongoose = require('mongoose')

const Schema = mongoose.Schema

const playerSchema = new Schema({
    tournament: {
        type: Schema.Types.ObjectId,
        ref: 'Tournament'
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    points: {
        type: Number,
        required: false
    },
    position: {
        type: Number,
        required: false
    },
    chessgames: [{
        type: Schema.Types.ObjectId,
        ref: 'Chessgame'
    }]
})

module.exports = mongoose.model('Player', playerSchema)