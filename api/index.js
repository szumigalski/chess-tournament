const express = require('express')
const bodyParser = require('body-parser')
const graphqlHttp = require('express-graphql')
const mongoose = require('mongoose')
const cors = require('cors')
const graphQlSchema = require('./graphql/schema/index')
const graphQlResolvers = require('./graphql/resolvers/index')
const isAuth = require('./middleware/isAuth')

const app = express()


app.use(bodyParser.json())
app.use(cors())
app.use(isAuth)

app.use('/graphql', graphqlHttp({
    schema: graphQlSchema,
    rootValue: graphQlResolvers,
    graphiql: true
}))

mongoose.connect(`mongodb://admin:admin1@ds219983.mlab.com:19983/chesstournament`)
    .then(() => app.listen(5000)
    )
    .catch(err => console.log(err) )

