const { buildSchema } = require('graphql')

module.exports = buildSchema(`
type Tournament {
    _id: ID!
    organizator: User
    name: String
    place: String
    contact: String
    countOfRounds: Int
    timeForPlayers: Int
    limitOfPlayers: Int
    otherInformations: String
    currentRound: Int
    date: String,
    participants: [Player]
    newRoundTime: String
    chessgames: [Chessgame]
}

type Chessgame {
    _id: ID!
    tournament: Tournament
    white: Player
    black: Player
    roundNumber: Int
    whiteResult: String
    blackResult: String
    result: String
    chessboardNumber: Int
}

"""New User to log in"""
type User {
    _id: ID!
    password: String
    name: String!
    category: Int
    players: [Player!]
    createdTournaments: [Tournament!]
}

type Player {
    _id: ID!
    user: User!
    tournament: Tournament!
    chessgames: [Chessgame!]
    points: Int
    position: Int
}

type AuthData {
    userId: ID!
    token: String
    tokenExpiration: Int!
}

input TournamentInput {
    place: String
    contact: String
    countOfRounds: Int
    timeForPlayers: Int
    limitOfPlayers: Int
    otherInformations: String
    currentRound: Int
    date: String
    newRoundTime: String
    organizator: String
    name: String
}

input UserInput {
    password: String!
    name: String!
    category: Int
}

input UserUpdate {
    name: String!
    category: Int
}

input PlayerUpdate {
    id: String
    points: Int
    position: Int
}

input ChessgameUpdate {
    id: String
    whiteResult: String
    blackResult: String
    result: String
}

input TournamentUpdate {
    id: String
    place: String
    contact: String
    countOfRounds: Int
    timeForPlayers: Int
    limitOfPlayers: Int
    otherInformations: String
    currentRound: Int
    date: String,
    newRoundTime: String
    name: String
}

input PlayerInput {
    points: Int
    user: String
    tournament: String
}

input ChessgameInput {
    white: String
    black: String
    roundNumber: Int
    tournament: String
    whiteResult: String
    blackResult: String
    result: String
    chessboardNumber: String
}

type RootQuery {
    tournaments: [Tournament!]!
    chessgames: [Chessgame!]!
    players: [Player!]!
    users: [User!]!
    login(name: String!, password: String!): AuthData!
}

type RootMutation {
    createTournament(tournamentInput: TournamentInput): Tournament
    createUser(userInput: UserInput): User
    createPlayer(playerInput: PlayerInput) : Player
    createChessgame(chessgameInput: ChessgameInput): Chessgame
    updateUser(userInput: UserUpdate): User
    updatePlayer(playerInput: PlayerUpdate): Player
    updateChessgame(chessgameInput: ChessgameUpdate): Chessgame
    updateTournament(tournamentInput: TournamentUpdate): Tournament
}

schema {
    query: RootQuery
    mutation: RootMutation
}
`)