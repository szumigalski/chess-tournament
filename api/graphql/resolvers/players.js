const User = require('../../models/user')
const Player = require('../../models/player')
const Tournament = require('../../models/tournament')


const { transformPlayer } = require('./merge')


module.exports = {
    players: async (args, req) => {
        if(!req.isAuth) {
            throw new Error('Unauthenticated')
        }
        try {
            const players = await Player.find()
                return players.map(player => {
                    return transformPlayer(player)
            })
        } catch (err) {
            throw err
        }
    },
    createPlayer: async (args, req) => {
        if(!req.isAuth) {
            throw new Error('Unauthenticated')
        }
        const player = new Player ({
            user: args.playerInput.user,
            points: args.playerInput.points,
            tournament: args.playerInput.tournament
        })
        let createdPlayer
        try {
            const user = await User.findById(args.playerInput.user)
            if(!user) {
                throw new Error('User is not found in database')
            }
            const tournament = await Tournament.findById(args.playerInput.tournament)
            if(!tournament) {
                throw new Error('Tournament is not found in database')
            }
            const result = await player.save()
            createdPlayer = transformPlayer(result)
            console.log(user.players)
            user.players.push(player)
            await user.save()
            tournament.participants.push(player)
            await tournament.save()
            return createdPlayer
    } catch (err) {
        throw err
    }
},
updatePlayer: async args => {
    try {
        const existingPlayer = await Player.findOne({ _id: args.playerInput.id})
        if (!existingPlayer) {
            throw new Error('Player not exist')
        }
        args.playerInput.points && (existingPlayer.points = args.playerInput.points)
        args.playerInput.position && (existingPlayer.position = args.playerInput.position)

        const result = await existingPlayer.save()

            return {
                ...result._doc,
                _id: result.id
            }
        } catch (err) {
            throw err
        }
}
}