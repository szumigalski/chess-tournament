const Chessgame = require('../../models/chessgame')
const Tournament = require('../../models/tournament')
const Player = require('../../models/player')

const { transformChessgame } = require('./merge')

module.exports = {
    chessgames: async (args, req) => {
        if(!req.isAuth) {
            throw new Error('Unauthenticated')
        }
        try {
            const chessgames = await Chessgame.find()
                return chessgames.map(chessgame => {
                    return transformChessgame(chessgame)
            })
        } catch (err) {
            throw err
        }
    },
    createChessgame: async (args, req) => {
        if(!req.isAuth) {
            throw new Error('Unauthenticated')
        }
        const chessgame = new Chessgame({
            white: args.chessgameInput.white,
            black: args.chessgameInput.black,
            tournament: args.chessgameInput.tournament,
            roundNumber: args.chessgameInput.roundNumber,
            whiteResult: args.chessgameInput.whiteResult,
            blackResult: args.chessgameInput.blackResult,
            result: args.chessgameInput.result,
            chessboardNumber: args.chessgameInput.chessboardNumber

        })
        let createdChessgame
        try {
            const result = await chessgame.save()
            createdChessgame = transformChessgame(result)
                const white = await Player.findById(args.chessgameInput.white)
                if (!white) {
                    throw new Error('Player white not found')
                }
                const black = await Player.findById(args.chessgameInput.black)
                if (!black) {
                    throw new Error('Player black not found')
                }
                const tournament = await Tournament.findById(args.chessgameInput.tournament)
                if (!tournament) {
                    throw new Error('Tournament not found')
                }
                white.chessgames.push(chessgame)
                await white.save()
                black.chessgames.push(chessgame)
                await black.save()
                tournament.chessgames.push(chessgame)
                await tournament.save()
                return createdChessgame
        } catch (err) {
            throw err
        }
    },
    updateChessgame: async args => {
        try {
            const existingChessgame = await Chessgame.findOne({ _id: args.chessgameInput.id})
            if (!existingChessgame) {
                throw new Error('Chessgame not exist')
            }
            args.chessgameInput.whiteResult && (existingChessgame.whiteResult = args.chessgameInput.whiteResult)
            args.chessgameInput.blackResult && (existingChessgame.blackResult = args.chessgameInput.blackResult)
            args.chessgameInput.result && (existingChessgame.result = args.chessgameInput.result)

            const result = await existingChessgame.save()

                return {
                    ...result._doc,
                    _id: result.id
                }
            } catch (err) {
                throw err
            }
    }
}