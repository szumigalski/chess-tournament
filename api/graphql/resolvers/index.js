const userResolver = require('./users')
const playerResolver = require('./players')
const tournamentResolver = require('./tournaments')
const chessgamesResolver = require('./chessgames')

const rootResolver = {
    ...userResolver,
    ...playerResolver,
    ...tournamentResolver,
    ...chessgamesResolver
}

module.exports = rootResolver