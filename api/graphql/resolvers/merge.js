const User = require('../../models/user')
const Tournament = require('../../models/tournament')
const Player = require('../../models/player')
const Chessgame = require('../../models/chessgame')

const tournaments = async tournamentIds => {
    try {
        const tournaments = await Tournament.find({ _id: { $in: tournamentIds}})
        return tournaments.map(tournament => {
            return transformTournament(tournament)
        })
    } catch (err) {
        throw err
    }
}

const players = async playersIds => {
    try {
        const players = await Player.find({ _id: { $in: playersIds}})
        return players.map(player => {
            return transformPlayer(player)
        })
    } catch (err) {
        throw err
    }
}

const chessgames = async chessgamesIds => {
    try {
        const chessgames = await Chessgame.find({ _id: { $in: chessgamesIds}})
        return chessgames.map(chessgame => {
            return transformChessgame(chessgame)
        })
    } catch (err) {
        throw err
    }
}

const user = async userId => {
    try {
        const user = await User.findById(userId)
        return {
            ...user._doc,
            _id: user.id,
            createdTournaments: tournaments.bind(this, user._doc.createdTournaments),
            players: players.bind(this, user._doc.players)
        }
    } catch (err) {
        throw err
    }
}

const player = async playerId => {
    try {
        const player = await Player.findById(playerId)
        return {
            ...player._doc,
            _id: player.id,
            user: user.bind(this, player._doc.user),
            tournament: tournament.bind(this, player._doc.tournament),
            chessgames: chessgames.bind(this, player._doc.chessgames)
        }
    } catch (err) {
        throw err
    }
}

const tournament = async tournamentId => {
    try {
        const tournament = await Tournament.findById(tournamentId)
        return {
            ...tournament._doc,
            _id: tournament.id,
            participants: players.bind(this, tournament._doc.participants),
            organizator: user.bind(this, tournament._doc.organizator),
            chessgames: chessgames.bind(this, tournament._doc.chessgames)
        }
    } catch (err) {
        throw err
    }
}

const chessgame = async chessgameId => {
    try {
        const chessgame = await Chessgame.findById(chessgameId)
        return {
            ...chessgame._doc,
            _id: chessgame.id,
            white: player.bind(this, chessgame._doc.white),
            black: player.bind(this, chessgame._doc.black),
            tournament: tournament.bind(this, chessgame._doc.tournament)
        }
    } catch (err) {
        throw err
    }
}

const transformTournament = tournament => {
    return {
        ...tournament._doc,
        _id: tournament.id,
        organizator: user.bind(this, tournament.organizator),
        participants: players.bind(this, tournament.participants),
        chessgames: chessgames.bind(this, tournament.chessgames)
    }
}

const transformPlayer = player => {
    return {
        ...player._doc,
        _id: player.id,
        user: user.bind(this, player.user),
        tournament: tournament.bind(this, player.tournament),
        chessgames: chessgames.bind(this, player.chessgames)
    }
}

const transformChessgame = chessgame => {
    return {
        ...chessgame._doc,
        _id: chessgame.id,
        white: player.bind(this, chessgame.white),
        black: player.bind(this, chessgame.black),
        tournament: tournament.bind(this, chessgame.tournament)
    }
}

exports.transformTournament = transformTournament
exports.tournaments = tournaments
exports.tournament = tournament
exports.user = user
exports.player = player
exports.chessgame = chessgame
exports.transformPlayer = transformPlayer
exports.transformChessgame = transformChessgame
