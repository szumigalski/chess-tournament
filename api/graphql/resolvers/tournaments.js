const Tournament = require('../../models/tournament')
const User = require('../../models/user')

const { transformTournament } = require('./merge')

module.exports = {
    tournaments: async (args, req) => {
        if(!req.isAuth) {
            throw new Error('Unauthenticated')
        }
        try {
            const tournaments = await Tournament.find()
                return tournaments.map(tournament => {
                    return transformTournament(tournament)
            })
        } catch (err) {
            throw err
        }
    },
    createTournament: async (args, req) => {
        if(!req.isAuth) {
            throw new Error('Unauthenticated')
        }
        const tournament = new Tournament({
            name: args.tournamentInput.name,
            organizator: args.tournamentInput.organizator,
            place: args.tournamentInput.place,
            contact: args.tournamentInput.contact,
            countOfRounds: args.tournamentInput.countOfRounds ? +args.tournamentInput.countOfRounds : 7,
            timeForPlayers: args.tournamentInput.timeForPlayers ? +args.tournamentInput.timeForPlayers : 15,
            limitOfPlayers: args.tournamentInput.limitOfPlayers ? +args.tournamentInput.limitOfPlayers : 30,
            otherInformations: args.tournamentInput.otherInformations,
            currentRound: 0,
            date: args.tournamentInput.date ? new Date(args.tournamentInput.date) : new Date()
        })
        let createdTournament
        try {
            const result = await tournament.save()
                createdTournament = transformTournament(result)
                const organizator = await User.findById(args.tournamentInput.organizator)
                if (!organizator) {
                    throw new Error('User not found')
                }
                organizator.createdTournaments.push(tournament)
                await organizator.save()
                return createdTournament
        } catch (err) {
            throw err
        }
    },
    updateTournament: async args => {
        try {
            const existingTournament = await Tournament.findOne({ _id: args.tournamentInput.id})
            if (!existingTournament) {
                throw new Error('Tournament not exist')
            }
            tournamentFields = [
                'place', 'contact', 'countOfRounds',
                'timeForPlayers', 'limitOfPlayers',
                'otherInformations', 'currentRound',
                'date', 'newRoundTime', 'name'
            ]
            tournamentFields.map(item => {
                args.tournamentInput[item] && (existingTournament[item] = args.tournamentInput[item])
            })

            const result = await existingTournament.save()

                return {
                    ...result._doc,
                    _id: result.id
                }
            } catch (err) {
                throw err
            }
    }
}