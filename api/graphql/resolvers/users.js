const User = require('../../models/user')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

module.exports = {
    users: async (args, req) => {
        if(!req.isAuth) {
            throw new Error('Unauthenticated')
        }
        try {
            const users = await User.find()
                return users.map(user => {
                    return {
                        name: user.name,
                        _id: user.id,
                        password: null,
                        category: user.category
                    }
            })
        } catch (err) {
            throw err
        }
    },
    createUser: async (args, req) => {
        try {
            const existingUser = await User.findOne({ name: args.userInput.name})
            if (existingUser) {
                throw new Error('User exist already')
            }
                const hashedPassword = await bcrypt.hash(args.userInput.password, 12)

                const user = new User({
                    name: args.userInput.name,
                    password: hashedPassword,
                    category: args.userInput.category
                })

                const result = await user.save()

                return {
                    ...result._doc,
                    password: null,
                    _id: result.id
                }
            } catch (err) {
                throw err
            }
    },
    updateUser: async args => {
        try {
            const existingUser = await User.findOne({ name: args.userInput.name})
            if (!existingUser) {
                throw new Error('User not exist')
            }
            args.userInput.category && (existingUser.category = args.userInput.category)

            const result = await existingUser.save()

                return {
                    ...result._doc,
                    password: null,
                    _id: result.id
                }
            } catch (err) {
                throw err
            }
    },
    login: async ({ name, password }) => {
        const user = await User.findOne({ name: name })
        if (!user) {
            throw new Error('User is not exist')
        }
        const isEqual = await bcrypt.compare(password, user.password)
        if (!isEqual) {
            throw new Error('Password is incorrect')
        }
        const token = jwt.sign(
            { userId: user.id, name: user.name},
            'chessssehc',
            {expiresIn: '1h'})
            return {
                userId: user.id,
                token,
                tokenExpiration: 1
            }
    }
}